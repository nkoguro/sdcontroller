/**
 *  fat.h - FAT Operation
 *
 *   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without 
 *   modification, are permitted provided that the following conditions 
 *   are met:
 *
 *   1. Redistributions of source code must retain the above copyright 
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright 
 *      notice, this list of conditions and the following disclaimer in the 
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the authors nor the names of its contributors 
 *      may be used to endorse or promote products derived from this 
 *      software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 *   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 *   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 *   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 *   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef SDCONTROLLER_FAT_H_
#define SDCONTROLLER_FAT_H_

namespace FAT {

typedef struct __attribute__ ((__packed__)) {
  uint8_t check_routine[446];
  struct __attribute__ ((__packed__)) {
    uint8_t bood_descriptor;
    uint8_t first_partition_sector[3];
    uint8_t first_system_descriptor;
    uint8_t last_partition_sector[3];
    uint32_t first_sector_number;
    uint32_t number_of_sectors;
  } partition_table[4];
  uint16_t signature;
} MBR;

typedef union {
  struct __attribute__ ((__packed__)) {
    uint8_t jmp_op_code[3];
    uint8_t oem_name[8];
    
    uint16_t bytes_per_sector;
    uint8_t sectors_per_cluster;
    uint16_t reserved_sectors;
    uint8_t number_of_fats;
    uint16_t root_entries;
    uint16_t total_sectors;
    uint8_t media_descriptor;
    uint16_t sectors_per_fat;
    uint16_t sectors_per_track;
    uint16_t heads;
    uint32_t hidden_sectors;
    uint32_t big_total_sectors;

    uint8_t drive_number;
    uint8_t unused;
    uint8_t ext_boot_signature;
    uint32_t serial_number;
    uint8_t volume_label[11];
    uint8_t file_system_type[8];
    uint8_t load_program_code[448];
    uint16_t signature;
  } fat16;
  struct __attribute__ ((__packed__)) {
    uint8_t jmp_op_code[3];
    uint8_t oem_name[8];
    
    uint16_t bytes_per_sector;
    uint8_t sectors_per_cluster;
    uint16_t reserved_sectors;
    uint8_t number_of_fats;
    uint16_t root_entries;
    uint16_t total_sectors;
    uint8_t media_descriptor;
    uint16_t sectors_per_fat;
    uint16_t sectors_per_track;
    uint16_t heads;
    uint32_t hidden_sectors;
    uint32_t big_total_sectors;
    uint32_t big_sectors_per_fat;
    uint16_t ext_flags;

    uint16_t fs_version;
    uint32_t root_dir_start_clusters;
    uint16_t fs_info_sec;
    uint16_t backup_boot_sector;
    uint8_t reserved[12];
    
    uint8_t drive_number;
    uint8_t unused;
    uint8_t ext_boot_signature;
    uint32_t serial_number;
    uint8_t volume_label[11];
    uint8_t file_system_type[8];
    uint8_t load_program_code[420];
    uint16_t signature;
  } fat32;
} BPB;

typedef struct __attribute__((__packed__)) {
  uint8_t name[8];
  uint8_t extension[3];
  uint8_t attribute;
  uint8_t reserved;
  uint8_t create_time_ms;
  uint16_t create_time;
  uint16_t create_date;
  uint16_t access_date;
  uint16_t cluster_high_word;
  uint16_t update_time;
  uint16_t update_date;
  uint16_t cluster;
  uint32_t file_size;
} DirEntry;

DirEntry *FindDirEntry(unsigned char *filename);
void ReadData(uint16_t start_cluster, uint32_t block);

#define ATTR_LFN 0x0f
#define ATTR_ARCHIVE 0x20
#define ATTR_DIRECTORY 0x10
#define ATTR_VOLUME 0x08
#define ATTR_HIDDEN 0x04
#define ATTR_SYSTEM 0x02
#define ATTR_READONLY 0x01

} // namespace FAT

#endif  // SDCONTROLLER_FAT_H_

/* -*- mode: c++-mode -*- */
/* end of file */
