/**
 *  sdspi.h - Low-level SD Card controller using SPI.
 *
 *   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without 
 *   modification, are permitted provided that the following conditions 
 *   are met:
 *
 *   1. Redistributions of source code must retain the above copyright 
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright 
 *      notice, this list of conditions and the following disclaimer in the 
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the authors nor the names of its contributors 
 *      may be used to endorse or promote products derived from this 
 *      software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 *   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 *   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 *   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 *   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef SDCONTROLLER_SDSPI_H_
#define SDCONTROLLER_SDSPI_H_

#include "status.h"

namespace SDSPI {

typedef union {
  unsigned char r1[1];
  unsigned char r2[2];
  unsigned char r3[5];
  unsigned char r7[5];
  unsigned char data_resp[1];
} Response;

extern unsigned char input_buffer[515];
extern unsigned char output_buffer[515];

extern Response *resp;

void InitModule(void *status_addr, void *resp_addr, void *buf_addr);
void InitSDCard();
void UpdateStatus();
void ReadSector(unsigned long sector);
void WriteSector(unsigned long sector);
void *GetBuffer();
bool IsCardDetected();
bool IsCardReady();

#define BUFFER_SIZE 515
}  // namespace SDSPI

#endif  // SDCONTROLLER_SDSPI_H_

/* -*- mode: c++-mode -*- */
/*  end of file */
