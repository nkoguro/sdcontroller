/**
 *  sdspi.cpp - Low-level SD Card controller using SPI.
 *
 *   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without 
 *   modification, are permitted provided that the following conditions 
 *   are met:
 *
 *   1. Redistributions of source code must retain the above copyright 
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright 
 *      notice, this list of conditions and the following disclaimer in the 
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the authors nor the names of its contributors 
 *      may be used to endorse or promote products derived from this 
 *      software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 *   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 *   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 *   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 *   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <avr/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#ifdef DEBUG
#include <HardwareSerial.h>
#endif

#include "sdspi.h"
#include "status.h"

namespace SDSPI {

unsigned char *buffer;
Response *resp;
Status *status;
char addr_shift;

const unsigned long k_timeout_counter_low = 250000;
const unsigned long k_timeout_counter_high = 4000000;

#ifdef DEBUG
static void print_hex2(unsigned char data) {
  if (data < 0x10) {
    Serial.print("0");
  }
  Serial.print(data, HEX);
}

static void print_hex4(unsigned int data) {
  if (data < 0x1000) {
    Serial.print("0");
  }
  if (data < 0x100) {
    Serial.print("0");
  }
  if (data < 0x10) {
    Serial.print("0");
  }
  Serial.print(data, HEX);
}

// size must be multiple of 16;
static void dump(const char *msg, unsigned int sect, unsigned char* buf, unsigned int size) {
  char text[17];

  Serial.print("*** ");
  Serial.print(msg);
  Serial.print("  (sector: ");
  Serial.print(sect, DEC);
  Serial.println(") ***");
  
  for (unsigned int addr = 0; addr < size; ++addr) {
    if ((addr % 0x10) == 0) {
      print_hex4(addr);
      Serial.print(": ");
    }
    print_hex2(buf[addr]);
    if (isprint(buf[addr])) {
      text[addr % 0x10] = buf[addr];
    } else {
      text[addr % 0x10] = '.';
    }
    Serial.print(" ");
    if ((addr % 0x10) == 0xf) {
      Serial.print("  ");
      text[16] = '\0';
      Serial.println(text);
    }
  }
  Serial.println("");
}

static void PrintResponse(unsigned char cmd, const char *resp_name, int n) {
  Serial.println("*** Command Result ***");
  Serial.print("Cmd: ");
  Serial.print(cmd, DEC);
  Serial.print(", ");
  Serial.print(resp_name);
  Serial.print("=0x");
  for (int i =  n - 1; 0 <= i; --i) {
    print_hex2(resp->r7[i]);
  }
  Serial.println("");
}

#define PRINT_R1(cmd) PrintResponse(cmd, "R1", sizeof(resp->r1) / sizeof(resp->r1[0]))
#define PRINT_R2(cmd) PrintResponse(cmd, "R2", sizeof(resp->r2) / sizeof(resp->r2[0]))
#define PRINT_R3(cmd) PrintResponse(cmd, "R3", sizeof(resp->r3) / sizeof(resp->r3[0]))
#define PRINT_R7(cmd) PrintResponse(cmd, "R7", sizeof(resp->r7) / sizeof(resp->r7[0]))
#define DUMP_BUFFER(msg, sect)  dump(msg, sect, &buffer[1], 512)

#else
#define PRINT_R1(cmd)
#define PRINT_R2(cmd)
#define PRINT_R3(cmd)
#define PRINT_R7(cmd)
#define DUMP_BUFFER(msg, sect)
#endif

inline void SetCSH() {
  PORTB |= _BV(PORTB2);
}

inline void SetCSL() {
  PORTB &= ~_BV(PORTB2);
}

inline void WaitSPI() {
  while (!(SPSR & (1 << SPIF)));
}

inline void SendSPIData(unsigned char data) {
  SPDR = data;
  WaitSPI();
}

inline void SendCMD(unsigned char cmd_num,
                    unsigned long arg,
                    unsigned char crc7) {
  SendSPIData(0x40 | cmd_num);
  SendSPIData(arg >> 24);
  SendSPIData(arg >> 16);
  SendSPIData(arg >>  8);
  SendSPIData(arg      );
  SendSPIData(crc7);
}

inline void SendData(unsigned char* buf, int size) {
  for (int i = 0; i < size; ++i) {
    SendSPIData(buf[i]);
  }
}

inline void RecvData(unsigned char* buf, int size, unsigned long timeout) {
  do {
    if (timeout == 0) {
      return;
    }
    SPDR = 0xff;
    --timeout;
    WaitSPI();
  } while ((buf[0] = SPDR) == 0xff);

  for (int i = 1; i < size; ++i) {
    SendSPIData(0xff);
    buf[i] = SPDR;
  }
}

inline void SkipBusy(unsigned long timeout) {
  do {
    if (timeout == 0) {
      return;
    }
    SPDR = 0xff;
    --timeout;
    WaitSPI();
  } while (SPDR != 0xff);
}

inline void RecvR1(unsigned char cmd, unsigned long timeout) {
  RecvData(resp->r1, sizeof(resp->r1), timeout);
  PRINT_R1(cmd);
}

inline void RecvR2(unsigned char cmd, unsigned long timeout) {
  RecvData(resp->r2, sizeof(resp->r2), timeout);
  PRINT_R2(cmd);
}

inline void RecvR3(unsigned char cmd, unsigned long timeout) {
  RecvData(resp->r3, sizeof(resp->r3), timeout);
  PRINT_R3(cmd);
}

inline void RecvR7(unsigned char cmd, unsigned long timeout) {
  RecvData(resp->r7, sizeof(resp->r7), timeout);
  PRINT_R7(cmd);
}

inline void SendDummyClock() {
  // nCS = H and send dummy 80 clocks.
  SetCSH();
  for (char i = 0; i < 10; ++i) {
    SendSPIData(0xff);
  }
}

inline void SendCMD0() {
  SetCSL();
  SendCMD(0, 0, 0x95);
  RecvR1(0, k_timeout_counter_low);
  SetCSH();
}

inline void SendCMD8() {
  SetCSL();
  SendCMD(8, 0, 0x87);
  RecvR7(8, k_timeout_counter_low);
  SetCSH();
}

inline void SendACMD41(char version) {
  while (true) {
    SetCSL();
    SendCMD(55, 0, 0x65);
    RecvR1(55, k_timeout_counter_low);
    if (resp->r1[0] != 0x01) {
      SetCSH();
      return;
    }

    if (version <= 1) {
      SendCMD(41, 0, 0xe5);
    } else {
      SendCMD(41, 0x40000000, 0x77);
    }
    RecvR1(41, k_timeout_counter_low);
    SetCSH();
    if (resp->r1[0] != 0x01) {
      return;
    } 
    _delay_ms(1000.0);
  }
}  

inline void SendCMD58() {
  SetCSL();
  SendCMD(58, 0, 0xfd);
  RecvR3(58, k_timeout_counter_low);
  SetCSH();
}

inline void SendCMD16() {
  SetCSL();
  SendCMD(16, 0x00000200, 0xff);
  RecvR1(16, k_timeout_counter_low);
  SetCSH();
}

inline void SendCMD17(unsigned long sector) {
  unsigned long arg = sector << addr_shift;
  SetCSL();
  SendCMD(17, arg, 0xff);
  RecvR1(17, k_timeout_counter_high);
  RecvData(buffer, BUFFER_SIZE, k_timeout_counter_high);
  SetCSH();
}

inline void SendCMD13() {
  SetCSL();
  SendCMD(13, 0, 0xff);
  RecvR2(13, k_timeout_counter_high);
  SetCSH();
}

inline void SendCMD24(unsigned long sector) {
  unsigned long arg = sector << addr_shift;
  buffer[0] = 0xfe; // Start block token
  buffer[513] = 0xff; // dummy CRC
  buffer[514] = 0xff; // dummy CRC
  SetCSL();
  SendCMD(24, arg, 0xff);
  RecvR1(24, k_timeout_counter_high);
  if (resp->r1[0] != 0x00) {
    goto exit;
  }
  SendData(buffer, BUFFER_SIZE);
  RecvData(resp->data_resp, sizeof(resp->data_resp), k_timeout_counter_high);
  SkipBusy(k_timeout_counter_high);

exit:
  SetCSH();
}

void InitModule(void *status_addr, void *resp_addr, void *buf_addr) {
  status = (Status*) status_addr;
  resp = (Response*) resp_addr;
  buffer = (unsigned char*) buf_addr;
  
  status->sdspi_busy = false;
  status->card_ready = false;
  // set output SCK(PB5), MOSI(PB3) and SS(PB2)
  DDRB = (1 << DDB5) | (1 << DDB3) | (1 << DDB2);
  SetCSH();
}

void InitSDCard() {
  status->sdspi_busy = true;
  status->card_ready = false;
  /*
    SPI Interrupt Enable : disable
    SPI Enable           : enable
    Data Order           : MSB of the data word is transmitted first.
    Master/Slave         : Master
    Clock Polarity       : SCK is high when idle.
    Clock Phase          : Leading Edge=Setup, Trailing Edge=Sample
    SPI Clock Rate       : f_osc/64 (250kHz)
   */
  SPCR = 0b01011110;
  SPSR = 0b00000000;

  // wait 1ms.
  _delay_ms(1.0);

  // nCS = H and send dummy 80 clocks.
  SendDummyClock();

  SendCMD0();
  if (resp->r1[0] != 0x01) {
    goto exit;
  }

  char version;
  SendCMD8();
  if (resp->r7[0] == 0x01) {
    version = 2;
  } if (resp->r7[0] == 0x05) {
    version = 1;
  } else {
    goto exit;
  }

  SendACMD41(version);
  if (resp->r1[0] != 0x00) {
    goto exit;
  }

  addr_shift = 9;
  if (version == 2) {
    SendCMD58();
    if (resp->r3[1] & 0b01000000) {
      // address unit is block if CCS = 1.
      addr_shift = 0;
    }
  }

  // Set block length to 512 bytes.
  SendCMD16();
  if (resp->r1[0] == 0x00) {
    status->card_ready = true;

    /*
      SPI Interrupt Enable : disable
      SPI Enable           : enable
      Data Order           : MSB of the data word is transmitted first.
      Master/Slave         : Master
      Clock Polarity       : SCK is high when idle.
      Clock Phase          : Leading Edge=Setup, Trailing Edge=Sample
      SPI Clock Rate       : f_osc/2 (8MHz)
    */
    SPCR = 0b01011100;
    SPSR = 0b00000001;
  }
  
exit:
  status->sdspi_busy = false;
}

void UpdateStatus() {
  if (PINB & _BV(PORTB0)) {
    // not detected
    status->card_detect = false;
    status->sdspi_busy = false;
    status->card_ready = false;
  } else {
    status->card_detect = true;
  }
}

void ReadSector(unsigned long sector) {
  SendCMD17(sector);
  DUMP_BUFFER("Read", sector);
}

void WriteSector(unsigned long sector) {
  DUMP_BUFFER("Write", sector);
  SendCMD24(sector);
  SendCMD13();
}

void *GetBuffer() {
  return &buffer[1];
}

bool IsCardDetected() {
  return status->card_detect;
}

bool IsCardReady() {
  return status->card_ready;
}

} // namespace SDSPI

/* end of file */
