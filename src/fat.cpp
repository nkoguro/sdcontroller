/**
 *  fat.cpp - 
 *
 *   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without 
 *   modification, are permitted provided that the following conditions 
 *   are met:
 *
 *   1. Redistributions of source code must retain the above copyright 
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright 
 *      notice, this list of conditions and the following disclaimer in the 
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the authors nor the names of its contributors 
 *      may be used to endorse or promote products derived from this 
 *      software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 *   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 *   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 *   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 *   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <avr/io.h>

#include "fat.h"
#include "sdspi.h"
#include "status.h"

namespace FAT {
static uint32_t fat_sector;
static uint16_t sectors_per_fat;
static uint32_t rde_sector;
static uint16_t sectors_per_rde;
static uint8_t sectors_per_cluster;

static const uint16_t kBytesPerSector = 512;
static const int kNameSize = sizeof(((DirEntry*) 0)->name);
static const int kExtSize = sizeof(((DirEntry*) 0)->extension);

static void Init() {
  PORTD = PORTD | _BV(PORTD7);
  SDSPI::InitSDCard();

  // Read MBR
  SDSPI::ReadSector(0);
  MBR *mbr = (MBR*) SDSPI::GetBuffer();
  unsigned long bpb_sector = mbr->partition_table[0].first_sector_number;

  // Read BPB
  SDSPI::ReadSector(bpb_sector);
  BPB *bpb = (BPB*) SDSPI::GetBuffer();
  sectors_per_cluster = bpb->fat16.sectors_per_cluster;
  fat_sector = bpb_sector + 1;
  sectors_per_fat = bpb->fat16.number_of_fats;
  rde_sector = fat_sector + bpb->fat16.sectors_per_fat * bpb->fat16.number_of_fats;
  sectors_per_rde = bpb->fat16.root_entries * sizeof(DirEntry) / kBytesPerSector;
  PORTD = PORTD & ~_BV(PORTD7);
}

static void GetCanonicalShortName(unsigned char *filename, unsigned char *name_buf) {
  memset(name_buf, ' ', kNameSize + kExtSize);
  for (int i = 0, j = 0; i < kNameSize + kExtSize; ++i, ++j) {
    switch (filename[j]) {
      case '\0':
        return;
      case '.':
        i = kNameSize - 1;
        break;
      default:
        name_buf[i] = toupper(filename[j]);
        break;
    }
  }
}

static bool Prepare() {
  SDSPI::UpdateStatus();
  if (!SDSPI::IsCardDetected()) {
    return false;
  }
  if (!SDSPI::IsCardReady()) {
    Init();
  }

  return SDSPI::IsCardReady();
}

DirEntry *FindDirEntry(unsigned char *filename) {
  if (!Prepare()) {
    return NULL;
  }
  
  if (rde_sector == 0) {
    return NULL;
  }

  unsigned char name_buf[kNameSize + kExtSize];
  GetCanonicalShortName(filename, name_buf);

  for (int i = 0; i < sectors_per_rde; ++i) {
    SDSPI::ReadSector(rde_sector + i);
    for (int j = 0; j < kBytesPerSector / sizeof(DirEntry); ++j) {
      DirEntry& entry = ((DirEntry*) SDSPI::GetBuffer())[j];
      if (entry.name[0] == 0) {
        return NULL;
      }
      if (entry.name[0] == 0xe5) {
        // This entry is deleted.
        continue;
      }
      if (entry.attribute == ATTR_LFN) {
        // This entry is LFN.
        continue;
      }
      if ((entry.attribute & (ATTR_DIRECTORY | ATTR_HIDDEN)) == 0) {
        if (memcmp(entry.name, name_buf, sizeof(name_buf)) == 0) {
          return &entry;
        }
      }
    }
  }
  return NULL;
}

static uint16_t GetNextCluster(uint16_t cluster, uint32_t *fat_pos) {
  uint32_t sector = fat_sector + (cluster >> 8);  // fat_sector + cluster * 2 / 512
  uint16_t offset = (cluster & 0xf) << 1;

  if (*fat_pos != sector) {
    SDSPI::ReadSector(sector);
    *fat_pos = sector;
  }

  uint16_t *fat = (uint16_t*) SDSPI::GetBuffer();
  
  return fat[offset];
}

static void GetClustersAndOffset(uint32_t sectors, uint16_t *clusters, uint8_t *offset) {
  *clusters = sectors / sectors_per_cluster;
  *offset = sectors - ((*clusters) * sectors_per_cluster);
}

void ReadData(uint16_t start_cluster, uint32_t block) {
  if (!Prepare()) {
    return;
  }
  
  uint16_t num_clusters;
  uint8_t offset;

  GetClustersAndOffset(block, &num_clusters, &offset);
  uint32_t fat_pos = 0;
  uint16_t cluster = start_cluster;
  for (int i = 0; i < num_clusters; ++i) {
    cluster = GetNextCluster(cluster, &fat_pos);
  }

  SDSPI::ReadSector(rde_sector + sectors_per_rde +
                    (cluster - 2) * sectors_per_cluster + offset);
}

}

/* end of file */
