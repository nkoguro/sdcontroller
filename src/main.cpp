#include <WProgram.h>

#include "fat.h"
#include "i2c_slave.h"
#include "sdspi.h"
#include "status.h"

#define STATUS_ADDR 0x00
#define CMD_ADDR 0x08
#define RESP_ADDR 0x10
#define ARG_ADDR 0x20
#define BUF_ADDR 0x1EF

#define CMD_IDLE 0x00
#define CMD_FIND_DIR_ENTRY 0x01
#define CMD_READ_DATA 0x02

static unsigned char mem[1024];
static Status *status;
static unsigned char *buffer;
static const uint8_t kSlaveAddr = 1;

#ifdef DEBUG
static void print_hex2(unsigned char data) {
  if (data < 0x10) {
    Serial.print("0");
  }
  Serial.print(data, HEX);
}

static void print_hex4(unsigned int data) {
  if (data < 0x1000) {
    Serial.print("0");
  }
  if (data < 0x100) {
    Serial.print("0");
  }
  if (data < 0x10) {
    Serial.print("0");
  }
  Serial.print(data, HEX);
}

// size must be multiple of 16;
static void dump(const char *msg, unsigned char* buf, unsigned int size) {
  char text[17];

  Serial.print("*** ");
  Serial.print(msg);
  Serial.println(" ***");
  
  for (unsigned int addr = 0; addr < size; ++addr) {
    if ((addr % 0x10) == 0) {
      print_hex4(addr);
      Serial.print(": ");
    }
    print_hex2(buf[addr]);
    if (isprint(buf[addr])) {
      text[addr % 0x10] = buf[addr];
    } else {
      text[addr % 0x10] = '.';
    }
    Serial.print(" ");
    if ((addr % 0x10) == 0xf) {
      Serial.print("  ");
      text[16] = '\0';
      Serial.println(text);
    }
  }
  Serial.println("");
}
#endif

void setup() {
  memset(mem, sizeof(mem), 0);
  status = (Status*) &mem[STATUS_ADDR];
  buffer = &mem[BUF_ADDR];
  DDRD = _BV(PORTD6) | _BV(PORTD7);
  PORTD = PORTD | _BV(PORTD7) | _BV(PORTD6);
  SDSPI::InitModule(status, &mem[RESP_ADDR], buffer - 1);
  I2C::Init(kSlaveAddr, mem, 2, sizeof(mem));

#ifdef DEBUG
  Serial.begin(9600);
  Serial.println("reset");
#endif
}

static void DispatchCommand() {
  switch (mem[CMD_ADDR]) {
    case CMD_FIND_DIR_ENTRY: {
#ifdef DEBUG
      Serial.println("exec FIND_DIR_ENTRY");
      dump("args", &mem[ARG_ADDR], 32);
#endif
      FAT::DirEntry *entry = FAT::FindDirEntry(&mem[ARG_ADDR]);
      if (entry) {
        mem[ARG_ADDR + 0] = entry->cluster >> 8;
        mem[ARG_ADDR + 1] = entry->cluster & 0xff;
        mem[ARG_ADDR + 2] = 0;
        mem[ARG_ADDR + 3] = 0;
        mem[ARG_ADDR + 4] = 0;
        mem[ARG_ADDR + 5] = 0;
        mem[ARG_ADDR + 6] = 0;
        mem[ARG_ADDR + 7] = 0;
        mem[ARG_ADDR + 8] = entry->file_size & 0xff;
        mem[ARG_ADDR + 9] = (entry->file_size >> 8) & 0xff;
        mem[ARG_ADDR + 10] = (entry->file_size >> 16) & 0xff;
        mem[ARG_ADDR + 11] = (entry->file_size >> 24) & 0xff;
      } else {
        mem[ARG_ADDR + 0] = 0;
        mem[ARG_ADDR + 1] = 0;
        mem[ARG_ADDR + 2] = 0;
        mem[ARG_ADDR + 3] = 0;
        mem[ARG_ADDR + 4] = 0;
        mem[ARG_ADDR + 5] = 0;
        mem[ARG_ADDR + 6] = 0;
        mem[ARG_ADDR + 7] = 0;
        mem[ARG_ADDR + 8] = 0;
        mem[ARG_ADDR + 9] = 0;
        mem[ARG_ADDR + 10] = 0;
        mem[ARG_ADDR + 11] = 0;
      }
      break;
    }
    case CMD_READ_DATA: {
#ifdef DEBUG
      Serial.println("exec READ_DATA");
      dump("args", &mem[ARG_ADDR], 32);
#endif
      uint16_t cluster = (mem[ARG_ADDR + 0] << 8) | mem[ARG_ADDR + 1];
      uint32_t block =
          (mem[ARG_ADDR + 2] << 24) |
          (mem[ARG_ADDR + 3] << 16) |
          (mem[ARG_ADDR + 4] << 8) |
          mem[ARG_ADDR + 5];
      if (cluster) {
        FAT::ReadData(cluster, block);
        ++block;
        mem[ARG_ADDR + 2] = (block >> 24);
        mem[ARG_ADDR + 3] = (block >> 16) & 0xff;
        mem[ARG_ADDR + 4] = (block >>  8) & 0xff;
        mem[ARG_ADDR + 5] = (block      ) & 0xff;
      }
      break;
    }
  }
  mem[CMD_ADDR] = CMD_IDLE;
}

void loop() {
  SDSPI::UpdateStatus();
  I2C::Exec();
  DispatchCommand();
}
