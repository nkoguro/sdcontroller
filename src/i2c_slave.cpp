/**
 *  i2c_slave.cpp - 
 *
 *   Copyright (c) 2011 KOGURO, Naoki (naoki@koguro.net)
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without 
 *   modification, are permitted provided that the following conditions 
 *   are met:
 *
 *   1. Redistributions of source code must retain the above copyright 
 *      notice, this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright 
 *      notice, this list of conditions and the following disclaimer in the 
 *      documentation and/or other materials provided with the distribution.
 *   3. Neither the name of the authors nor the names of its contributors 
 *      may be used to endorse or promote products derived from this 
 *      software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 *   TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
 *   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 *   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
 *   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 *   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <avr/io.h>

#include "i2c_slave.h"

namespace I2C {

static unsigned char *memory;
static uint16_t memory_size;
static uint8_t memory_addr_size;
static uint16_t memory_addr;

static enum {
  ADDR_LO,
  ADDR_HI,
  DATA,
} receive_status;

void Init(uint8_t slave_addr, void* mem, uint8_t mem_addr_size, uint16_t mem_size) {
  memory = (unsigned char*) mem;
  memory_addr_size = mem_addr_size;
  memory_size = mem_size;
  memory_addr = 0;
  if (mem_addr_size == 1) {
    receive_status = ADDR_LO;
  } else {
    receive_status = ADDR_HI;
  }
  
  TWAR = (slave_addr << 1);
  TWCR = _BV(TWEA) | _BV(TWEN);
}

static inline void IncrementAddress() {
  memory_addr = (memory_addr + 1) % memory_size;
}

static void ReceiveData(uint8_t data) {
  switch (receive_status) {
    case ADDR_LO:
      memory_addr = memory_addr | data;
      receive_status = DATA;
      break;
    case ADDR_HI:
      memory_addr = data << 8;
      receive_status = ADDR_LO;
      break;
    case DATA:
      memory[memory_addr] = data;
      IncrementAddress();
      break;
  }
}

static void TransmitData() {
  TWDR = memory[memory_addr];
  IncrementAddress();
}

void Exec() {
  if (!(TWCR & _BV(TWINT))) {
    // no data
    return;
  }

  PORTD = PORTD & ~_BV(PORTD6);

  switch (TWSR & 0b11111000) {
    case 0x60:
      // SLA+W received.
      switch (memory_addr_size) {
        case 1:
          receive_status = ADDR_LO;
          break;
        case 2:
          receive_status = ADDR_HI;
          break;
        default:
          memory_addr = 0;
          receive_status = DATA;
          break;
      }
      break;
    case 0x80:
      // data has been received.
      ReceiveData(TWDR);
      break;
    case 0xa8:
      // SLA+R received.
      if (memory_addr_size == 0) {
        memory_addr = 0;
      }
      TransmitData();
      break;
    case 0xb8:
      // data has been transmitted and ACK has been received.
      TransmitData();
      break;
    case 0xc0:
    case 0xa0:
      break;
  }
  PORTD = PORTD | _BV(PORTD6);
  TWCR = _BV(TWINT) | _BV(TWEA) | _BV(TWEN);
}

}  // namespace I2C

/* end of file */
