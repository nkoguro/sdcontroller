#!/bin/sh

target=$1
src_dir=$2

library="../lib/$target.a"

objs=""
srcs=""
incdirs=""

for fn in `(cd $src_dir; find . -name "*.c" -or -name "*.cpp")`; do
    srcs="$srcs $fn"
    incdirs="$incdirs -I$src_dir/`dirname $fn`"
done

incdirs=`echo $incdirs | tr ' ' '\n' | sort | uniq | tr '\n' ' '`

for srcfn in $srcs; do
    objfn="../lib/${target}_`basename $srcfn | sed -e 's/[^.]*$//'`o"
    objs="$objs $objfn"
    echo "$objfn : $src_dir/$srcfn"
    echo "	\$(CC_FOR_LIB) -c \$< -o \$@ $incdirs"
    echo ""
done

cat <<EOS
$library: $objs
	\$(AR) \$@ $^

LIBRARIES := \$(LIBRARIES) $library
EOS

