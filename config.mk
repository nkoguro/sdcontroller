BASE_DIR = /Applications/Arduino.app/Contents/Resources/Java
BIN_DIR = $(BASE_DIR)/hardware/tools/avr/bin
CORES_DIR = $(BASE_DIR)/hardware/arduino/cores/arduino
LIBS_DIR = $(BASE_DIR)/libraries
ETC_DIR = $(BASE_DIR)/hardware/tools/avr/etc

CC = $(BIN_DIR)/avr-gcc $(CPP_FLAGS) $(INCLUDES)
CC_FOR_LIB = $(BIN_DIR)/avr-gcc $(CPP_FLAGS) -I$(CORES_DIR)
LD = $(BIN_DIR)/avr-gcc $(LD_FLAGS)
AR = $(BIN_DIR)/avr-ar $(AR_FLAGS)
OBJCP = $(BIN_DIR)/avr-objcopy
SIZE = $(BIN_DIR)/avr-size -A --mcu=$(MCU)
AVRDUDE = $(BIN_DIR)/avrdude

LIBRARIES =
OBJECTS =
INCLUDES = -I$(CORES_DIR) -I.
DEFINES = -DF_CPU=$(F_CPU)L -DARDUINO=18

CPP_FLAGS = -g -Os -w -fno-exceptions -ffunction-sections -fdata-sections -mmcu=$(MCU) $(DEFINES)
LD_FLAGS = -Os -Wl,--gc-sections -mmcu=$(MCU)
AR_FLAGS = rcs

