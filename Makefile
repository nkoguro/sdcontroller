include config.mk

.PHONY: all upload clean dist-clean maintainer-clean lib

all:
	(cd src; make all)

upload:
	(cd src; make upload)

clean:
	(cd src; make clean)

dist-clean: clean
	rm -Rf lib

maintainer-clean: dist-clean

lib:
	rm -Rf lib
	mkdir lib
	sh ./genmk.sh arduino $(CORES_DIR) > lib/arduino.mk
	for d in `(cd $(LIBS_DIR); find . -type d -depth 1)`; do \
	    modnam=`basename $$d`; \
	    sh ./genmk.sh $$modnam $(LIBS_DIR)/$$modnam > lib/$$modnam.mk; \
	done
